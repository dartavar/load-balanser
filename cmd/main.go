// cmd/main/main.go

package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/dartavar/balanser/internal/config"
	"gitlab.com/dartavar/balanser/internal/server/router"
)

func main() {

	r := router.NewRouter()

	port := config.LoadConfig().Port

	portStr := fmt.Sprintf(":%s", strconv.Itoa(port))
	// Запуск HTTP-сервера на порту
	fmt.Println("Starting server on" + portStr)
	log.Fatal(http.ListenAndServe(portStr, r))
}
